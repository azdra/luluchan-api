-- MariaDB dump 10.18  Distrib 10.5.8-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: luluchan
-- ------------------------------------------------------
-- Server version	10.5.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` varchar(255) NOT NULL,
  `users` varchar(255) NOT NULL,
  `servers` varchar(255) NOT NULL,
  `activityName` varchar(255) NOT NULL,
  `activityType` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES ('767824187779710986','1','2','It\'s time to become a Chinese farmer','PLAYING','idle');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commands`
--

DROP TABLE IF EXISTS `commands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `uses` varchar(255) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_87632c6d4596995f1346b23c0c` (`name`),
  KEY `FK_875fa8a8d26a1ef24faf70224d4` (`type`),
  CONSTRAINT `FK_875fa8a8d26a1ef24faf70224d4` FOREIGN KEY (`type`) REFERENCES `commands_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commands`
--

LOCK TABLES `commands` WRITE;
/*!40000 ALTER TABLE `commands` DISABLE KEYS */;
INSERT INTO `commands` VALUES (1,'karma','0',5),(2,'pout','0',5),(3,'hug','0',5),(4,'cuddle','0',5),(5,'kiss','0',5),(6,'slap','0',5),(7,'wasted','0',5),(8,'bite','0',5),(9,'wink','0',5),(10,'bang','0',5),(11,'pat','0',5),(12,'lick','0',5),(13,'greet','0',5),(14,'thumbup','0',5),(15,'thumbdown','0',5),(16,'bleh','0',5),(17,'highfive','0',5),(18,'handholding','0',5),(19,'bless','0',5),(20,'ticlke','0',5),(21,'baka','0',5),(22,'poke','0',5),(23,'feed','0',5),(24,'burn','0',5),(25,'wrestling','0',5),(26,'push','0',5),(27,'punch','0',5),(28,'hit','0',5),(29,'naruto','0',5),(30,'ass','0',4),(31,'smaltits','0',4),(32,'bigtits','0',4),(33,'boobs','0',4),(34,'pussy','0',4),(35,'anal','0',4),(37,'mnude','0',4),(38,'fnude','0',4),(39,'lesbian','0',4),(40,'bdsm','0',4),(41,'feet','0',4),(42,'pee','0',4),(43,'lingerie','0',4),(44,'swimsuit','0',4),(45,'bondage','0',4),(46,'ahegaoreal','0',4),(47,'bixexy','0',4),(48,'toys','0',4),(49,'gay','0',4),(50,'yaoi','0',2),(51,'tentai','0',2),(52,'chiisai','0',2),(53,'hentai','0',2),(54,'hboobs','0',2),(55,'hsolo','0',2),(56,'hanal','0',2),(57,'yuri','0',2),(58,'keta','0',2),(59,'neko','0',2),(60,'futanari','0',2),(61,'ahegao','0',2),(62,'hswimsuit','0',2),(63,'cat','0',3),(64,'dog','0',3),(65,'panda','0',3),(66,'redpanda','0',3),(67,'bird','0',3),(68,'fox','0',3),(69,'koala','0',3),(70,'pika','0',3),(71,'wolf','0',3),(72,'alpaga','0',3),(73,'capybaras','0',3),(74,'dikdik','0',3),(75,'foodporn','0',3),(76,'corona','0',3),(77,'cosplay','0',3),(78,'macron','0',3),(79,'vitality','0',3),(80,'donald','0',3),(81,'pangolin','0',3),(82,'spider','0',3),(83,'lama','0',3),(84,'kiwi','0',3),(85,'duck','0',3),(86,'fennec','0',3),(87,'renard','0',3),(88,'octopus','0',3),(89,'ferret','0',3),(90,'snake','0',3),(91,'otter','0',3),(92,'quokka','0',3),(93,'raccoon','0',3),(94,'dinosaur','0',3),(95,'kitten','0',3),(96,'bees','0',3),(97,'horse','0',3),(98,'donkey','0',3),(99,'meme','0',3),(100,'baguette','0',3),(101,'chicken','0',3),(102,'comics','0',3),(103,'manga','0',3),(104,'bear','0',3),(105,'plush','0',3),(106,'fish','0',3),(107,'tanuki','0',3),(108,'shrug','0',1),(109,'cry','0',1),(110,'think','0',1),(111,'sleep','0',1),(112,'hungry','0',1),(113,'rage','0',1),(114,'nom','0',1),(115,'shy','0',1),(116,'laugh','0',1),(117,'smug','0',1),(118,'blush','0',1),(119,'sip','0',1),(120,'nyan','0',1),(121,'happy','0',1),(122,'bullshit','0',1);
/*!40000 ALTER TABLE `commands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commands_reaction`
--

DROP TABLE IF EXISTS `commands_reaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commands_reaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) NOT NULL,
  `member` varchar(255) NOT NULL,
  `uses` int(11) NOT NULL DEFAULT 1,
  `commandId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_d482ed73c00f3a34a55cca0bc73` (`commandId`),
  CONSTRAINT `FK_d482ed73c00f3a34a55cca0bc73` FOREIGN KEY (`commandId`) REFERENCES `commands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commands_reaction`
--

LOCK TABLES `commands_reaction` WRITE;
/*!40000 ALTER TABLE `commands_reaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `commands_reaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commands_type`
--

DROP TABLE IF EXISTS `commands_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commands_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_30e052f0afb256d2ee8d0a67a9` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commands_type`
--

LOCK TABLES `commands_type` WRITE;
/*!40000 ALTER TABLE `commands_type` DISABLE KEYS */;
INSERT INTO `commands_type` VALUES (1,'emote'),(2,'hentai'),(3,'image'),(4,'porn'),(5,'reaction');
/*!40000 ALTER TABLE `commands_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds`
--

DROP TABLE IF EXISTS `guilds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guild` varchar(255) NOT NULL,
  `setting` int(11) DEFAULT NULL,
  `audit` int(11) DEFAULT NULL,
  `log` int(11) DEFAULT NULL,
  `createAt` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `updateAt` datetime(6) DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_35b8779b03eca10fd36ee3a0ec` (`setting`),
  UNIQUE KEY `REL_6de3ba12abe1ffcf20d2d2d617` (`audit`),
  UNIQUE KEY `REL_21f2080972acaeb5b335405dca` (`log`),
  CONSTRAINT `FK_21f2080972acaeb5b335405dca9` FOREIGN KEY (`log`) REFERENCES `guilds_log` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_35b8779b03eca10fd36ee3a0ec2` FOREIGN KEY (`setting`) REFERENCES `guilds_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_6de3ba12abe1ffcf20d2d2d6175` FOREIGN KEY (`audit`) REFERENCES `guilds_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds`
--

LOCK TABLES `guilds` WRITE;
/*!40000 ALTER TABLE `guilds` DISABLE KEYS */;
/*!40000 ALTER TABLE `guilds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_audit`
--

DROP TABLE IF EXISTS `guilds_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guildUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `channelCreate` tinyint(4) NOT NULL DEFAULT 0,
  `channelUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `channelDelete` tinyint(4) NOT NULL DEFAULT 0,
  `channelOverwriteCreate` tinyint(4) NOT NULL DEFAULT 0,
  `channelOverwriteUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `channelOverwriteDelete` tinyint(4) NOT NULL DEFAULT 0,
  `memberKick` tinyint(4) NOT NULL DEFAULT 0,
  `memberPrune` tinyint(4) NOT NULL DEFAULT 0,
  `memberBanAdd` tinyint(4) NOT NULL DEFAULT 0,
  `memberBanRemove` tinyint(4) NOT NULL DEFAULT 0,
  `memberUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `memberRoleUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `memberMove` tinyint(4) NOT NULL DEFAULT 0,
  `memberDisconnect` tinyint(4) NOT NULL DEFAULT 0,
  `botAdd` tinyint(4) NOT NULL DEFAULT 0,
  `roleCreate` tinyint(4) NOT NULL DEFAULT 0,
  `roleUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `roleDelete` tinyint(4) NOT NULL DEFAULT 0,
  `inviteCreate` tinyint(4) NOT NULL DEFAULT 0,
  `inviteDelete` tinyint(4) NOT NULL DEFAULT 0,
  `webhookCreate` tinyint(4) NOT NULL DEFAULT 0,
  `webhookUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `webhookCDelete` tinyint(4) NOT NULL DEFAULT 0,
  `emojiCreate` tinyint(4) NOT NULL DEFAULT 0,
  `emojiUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `emojiDelete` tinyint(4) NOT NULL DEFAULT 0,
  `messageDelete` tinyint(4) NOT NULL DEFAULT 0,
  `messageBulkDelete` tinyint(4) NOT NULL DEFAULT 0,
  `messagePin` tinyint(4) NOT NULL DEFAULT 0,
  `messageUnPin` tinyint(4) NOT NULL DEFAULT 0,
  `integrationCreate` tinyint(4) NOT NULL DEFAULT 0,
  `integrationUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `integrationDelete` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_audit`
--

LOCK TABLES `guilds_audit` WRITE;
/*!40000 ALTER TABLE `guilds_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `guilds_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_log`
--

DROP TABLE IF EXISTS `guilds_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `other` varchar(255) DEFAULT NULL,
  `logJoinId` int(11) DEFAULT NULL,
  `logLeaveId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_a9cea788a92dbf7543f84c90e2` (`logJoinId`),
  UNIQUE KEY `IDX_76e1506e0377b302fe615ba8a0` (`logLeaveId`),
  UNIQUE KEY `REL_a9cea788a92dbf7543f84c90e2` (`logJoinId`),
  UNIQUE KEY `REL_76e1506e0377b302fe615ba8a0` (`logLeaveId`),
  CONSTRAINT `FK_76e1506e0377b302fe615ba8a0a` FOREIGN KEY (`logLeaveId`) REFERENCES `guilds_log_leave` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_a9cea788a92dbf7543f84c90e2f` FOREIGN KEY (`logJoinId`) REFERENCES `guilds_log_join` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_log`
--

LOCK TABLES `guilds_log` WRITE;
/*!40000 ALTER TABLE `guilds_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `guilds_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_log_join`
--

DROP TABLE IF EXISTS `guilds_log_join`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_log_join` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(4) NOT NULL DEFAULT 0,
  `message` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_log_join`
--

LOCK TABLES `guilds_log_join` WRITE;
/*!40000 ALTER TABLE `guilds_log_join` DISABLE KEYS */;
/*!40000 ALTER TABLE `guilds_log_join` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_log_leave`
--

DROP TABLE IF EXISTS `guilds_log_leave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_log_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(4) NOT NULL DEFAULT 0,
  `message` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_log_leave`
--

LOCK TABLES `guilds_log_leave` WRITE;
/*!40000 ALTER TABLE `guilds_log_leave` DISABLE KEYS */;
/*!40000 ALTER TABLE `guilds_log_leave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_settings`
--

DROP TABLE IF EXISTS `guilds_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefix` varchar(255) NOT NULL DEFAULT 'l!',
  `color` varchar(255) NOT NULL DEFAULT '#98344d',
  `lang` varchar(255) NOT NULL DEFAULT 'en',
  `log` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_settings`
--

LOCK TABLES `guilds_settings` WRITE;
/*!40000 ALTER TABLE `guilds_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `guilds_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_warn`
--

DROP TABLE IF EXISTS `guilds_warn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_warn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guild` int(11) DEFAULT NULL,
  `user` varchar(255) NOT NULL,
  `moderator` varchar(255) NOT NULL,
  `delete` tinyint(4) NOT NULL DEFAULT 0,
  `reason` varchar(100) NOT NULL,
  `createAt` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `updateAt` datetime(6) DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `case` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_064df3465df984bd627739be047` (`guild`),
  CONSTRAINT `FK_064df3465df984bd627739be047` FOREIGN KEY (`guild`) REFERENCES `guilds` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_warn`
--

LOCK TABLES `guilds_warn` WRITE;
/*!40000 ALTER TABLE `guilds_warn` DISABLE KEYS */;
/*!40000 ALTER TABLE `guilds_warn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `format` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images_commands`
--

DROP TABLE IF EXISTS `images_commands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images_commands` (
  `imagesId` int(11) NOT NULL,
  `commandsId` int(11) NOT NULL,
  PRIMARY KEY (`imagesId`,`commandsId`),
  KEY `IDX_9860a2b24d3c5c6e9f5a733fe7` (`imagesId`),
  KEY `IDX_b743ee576aa0116a46ad948518` (`commandsId`),
  CONSTRAINT `FK_9860a2b24d3c5c6e9f5a733fe74` FOREIGN KEY (`imagesId`) REFERENCES `images` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_b743ee576aa0116a46ad9485185` FOREIGN KEY (`commandsId`) REFERENCES `commands` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images_commands`
--

LOCK TABLES `images_commands` WRITE;
/*!40000 ALTER TABLE `images_commands` DISABLE KEYS */;
/*!40000 ALTER TABLE `images_commands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL,
  `blacklist` tinyint(4) NOT NULL,
  `role` varchar(255) NOT NULL,
  `createAt` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `updateAt` datetime(6) DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-30 11:18:24
