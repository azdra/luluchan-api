-- MariaDB dump 10.19  Distrib 10.5.10-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: luluchan
-- ------------------------------------------------------
-- Server version	10.5.10-MariaDB-1:10.5.10+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` varchar(255) NOT NULL,
  `activityName` varchar(255) NOT NULL,
  `activityType` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES ('767824187779710986','It\'s time to become a Chinese farmer','PLAYING','idle');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commands`
--

DROP TABLE IF EXISTS `commands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `visible` tinyint(4) NOT NULL DEFAULT 0,
  `uses` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_87632c6d4596995f1346b23c0c` (`name`),
  KEY `FK_875fa8a8d26a1ef24faf70224d4` (`type`),
  CONSTRAINT `FK_875fa8a8d26a1ef24faf70224d4` FOREIGN KEY (`type`) REFERENCES `commands_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commands`
--

LOCK TABLES `commands` WRITE;
/*!40000 ALTER TABLE `commands` DISABLE KEYS */;
INSERT INTO `commands` VALUES (1,'karma',2,0,0);
/*!40000 ALTER TABLE `commands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commands_reaction`
--

DROP TABLE IF EXISTS `commands_reaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commands_reaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) NOT NULL,
  `member` varchar(255) NOT NULL,
  `uses` int(11) NOT NULL DEFAULT 1,
  `commandId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_d482ed73c00f3a34a55cca0bc73` (`commandId`),
  CONSTRAINT `FK_d482ed73c00f3a34a55cca0bc73` FOREIGN KEY (`commandId`) REFERENCES `commands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commands_reaction`
--

LOCK TABLES `commands_reaction` WRITE;
/*!40000 ALTER TABLE `commands_reaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `commands_reaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commands_type`
--

DROP TABLE IF EXISTS `commands_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commands_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_30e052f0afb256d2ee8d0a67a9` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commands_type`
--

LOCK TABLES `commands_type` WRITE;
/*!40000 ALTER TABLE `commands_type` DISABLE KEYS */;
INSERT INTO `commands_type` VALUES (1,'emote'),(4,'hentai'),(3,'image'),(5,'porn'),(2,'reaction');
/*!40000 ALTER TABLE `commands_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds`
--

DROP TABLE IF EXISTS `guilds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guild` varchar(255) NOT NULL,
  `setting` int(11) DEFAULT NULL,
  `audit` int(11) DEFAULT NULL,
  `log` int(11) DEFAULT NULL,
  `createAt` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `updateAt` datetime(6) DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_35b8779b03eca10fd36ee3a0ec` (`setting`),
  UNIQUE KEY `REL_6de3ba12abe1ffcf20d2d2d617` (`audit`),
  UNIQUE KEY `REL_21f2080972acaeb5b335405dca` (`log`),
  CONSTRAINT `FK_21f2080972acaeb5b335405dca9` FOREIGN KEY (`log`) REFERENCES `guilds_log` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_35b8779b03eca10fd36ee3a0ec2` FOREIGN KEY (`setting`) REFERENCES `guilds_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_6de3ba12abe1ffcf20d2d2d6175` FOREIGN KEY (`audit`) REFERENCES `guilds_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds`
--

LOCK TABLES `guilds` WRITE;
/*!40000 ALTER TABLE `guilds` DISABLE KEYS */;
INSERT INTO `guilds` VALUES (1,'587424467538673664',1,1,1,'2021-09-04 20:46:17.991402','2021-09-04 20:46:17.991402');
/*!40000 ALTER TABLE `guilds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_audit`
--

DROP TABLE IF EXISTS `guilds_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guildUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `channelCreate` tinyint(4) NOT NULL DEFAULT 0,
  `channelUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `channelDelete` tinyint(4) NOT NULL DEFAULT 0,
  `channelOverwriteCreate` tinyint(4) NOT NULL DEFAULT 0,
  `channelOverwriteUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `channelOverwriteDelete` tinyint(4) NOT NULL DEFAULT 0,
  `memberKick` tinyint(4) NOT NULL DEFAULT 0,
  `memberPrune` tinyint(4) NOT NULL DEFAULT 0,
  `memberBanAdd` tinyint(4) NOT NULL DEFAULT 0,
  `memberBanRemove` tinyint(4) NOT NULL DEFAULT 0,
  `memberUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `memberRoleUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `memberMove` tinyint(4) NOT NULL DEFAULT 0,
  `memberDisconnect` tinyint(4) NOT NULL DEFAULT 0,
  `botAdd` tinyint(4) NOT NULL DEFAULT 0,
  `roleCreate` tinyint(4) NOT NULL DEFAULT 0,
  `roleUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `roleDelete` tinyint(4) NOT NULL DEFAULT 0,
  `inviteCreate` tinyint(4) NOT NULL DEFAULT 0,
  `inviteDelete` tinyint(4) NOT NULL DEFAULT 0,
  `webhookCreate` tinyint(4) NOT NULL DEFAULT 0,
  `webhookUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `webhookCDelete` tinyint(4) NOT NULL DEFAULT 0,
  `emojiCreate` tinyint(4) NOT NULL DEFAULT 0,
  `emojiUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `emojiDelete` tinyint(4) NOT NULL DEFAULT 0,
  `messageDelete` tinyint(4) NOT NULL DEFAULT 0,
  `messageBulkDelete` tinyint(4) NOT NULL DEFAULT 0,
  `messagePin` tinyint(4) NOT NULL DEFAULT 0,
  `messageUnPin` tinyint(4) NOT NULL DEFAULT 0,
  `integrationCreate` tinyint(4) NOT NULL DEFAULT 0,
  `integrationUpdate` tinyint(4) NOT NULL DEFAULT 0,
  `integrationDelete` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_audit`
--

LOCK TABLES `guilds_audit` WRITE;
/*!40000 ALTER TABLE `guilds_audit` DISABLE KEYS */;
INSERT INTO `guilds_audit` VALUES (1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `guilds_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_log`
--

DROP TABLE IF EXISTS `guilds_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `other` varchar(255) DEFAULT NULL,
  `logJoinId` int(11) DEFAULT NULL,
  `logLeaveId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_a9cea788a92dbf7543f84c90e2` (`logJoinId`),
  UNIQUE KEY `REL_76e1506e0377b302fe615ba8a0` (`logLeaveId`),
  CONSTRAINT `FK_76e1506e0377b302fe615ba8a0a` FOREIGN KEY (`logLeaveId`) REFERENCES `guilds_log_leave` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_a9cea788a92dbf7543f84c90e2f` FOREIGN KEY (`logJoinId`) REFERENCES `guilds_log_join` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_log`
--

LOCK TABLES `guilds_log` WRITE;
/*!40000 ALTER TABLE `guilds_log` DISABLE KEYS */;
INSERT INTO `guilds_log` VALUES (1,NULL,1,1);
/*!40000 ALTER TABLE `guilds_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_log_join`
--

DROP TABLE IF EXISTS `guilds_log_join`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_log_join` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(4) NOT NULL DEFAULT 0,
  `message` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_log_join`
--

LOCK TABLES `guilds_log_join` WRITE;
/*!40000 ALTER TABLE `guilds_log_join` DISABLE KEYS */;
INSERT INTO `guilds_log_join` VALUES (1,0,NULL,NULL);
/*!40000 ALTER TABLE `guilds_log_join` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_log_leave`
--

DROP TABLE IF EXISTS `guilds_log_leave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_log_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(4) NOT NULL DEFAULT 0,
  `message` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_log_leave`
--

LOCK TABLES `guilds_log_leave` WRITE;
/*!40000 ALTER TABLE `guilds_log_leave` DISABLE KEYS */;
INSERT INTO `guilds_log_leave` VALUES (1,0,NULL,NULL);
/*!40000 ALTER TABLE `guilds_log_leave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_settings`
--

DROP TABLE IF EXISTS `guilds_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefix` varchar(255) NOT NULL DEFAULT 'l!',
  `color` varchar(255) NOT NULL DEFAULT '#98344d',
  `lang` varchar(255) NOT NULL DEFAULT 'en',
  `log` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_settings`
--

LOCK TABLES `guilds_settings` WRITE;
/*!40000 ALTER TABLE `guilds_settings` DISABLE KEYS */;
INSERT INTO `guilds_settings` VALUES (1,'l!','#98344d','en',0);
/*!40000 ALTER TABLE `guilds_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guilds_warn`
--

DROP TABLE IF EXISTS `guilds_warn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guilds_warn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guild` int(11) DEFAULT NULL,
  `user` varchar(255) NOT NULL,
  `moderator` varchar(255) NOT NULL,
  `delete` tinyint(4) NOT NULL DEFAULT 0,
  `reason` varchar(100) NOT NULL,
  `createAt` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `updateAt` datetime(6) DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `case` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_064df3465df984bd627739be047` (`guild`),
  CONSTRAINT `FK_064df3465df984bd627739be047` FOREIGN KEY (`guild`) REFERENCES `guilds` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guilds_warn`
--

LOCK TABLES `guilds_warn` WRITE;
/*!40000 ALTER TABLE `guilds_warn` DISABLE KEYS */;
/*!40000 ALTER TABLE `guilds_warn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `format` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'reaction/karma/aYd5uRKSu0SyM4tVRhj_X.jpg',72761,'image/jpeg','aYd5uRKSu0SyM4tVRhj_X');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images_commands`
--

DROP TABLE IF EXISTS `images_commands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images_commands` (
  `imagesId` int(11) NOT NULL,
  `commandsId` int(11) NOT NULL,
  PRIMARY KEY (`imagesId`,`commandsId`),
  KEY `IDX_9860a2b24d3c5c6e9f5a733fe7` (`imagesId`),
  KEY `IDX_b743ee576aa0116a46ad948518` (`commandsId`),
  CONSTRAINT `FK_9860a2b24d3c5c6e9f5a733fe74` FOREIGN KEY (`imagesId`) REFERENCES `images` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_b743ee576aa0116a46ad9485185` FOREIGN KEY (`commandsId`) REFERENCES `commands` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images_commands`
--

LOCK TABLES `images_commands` WRITE;
/*!40000 ALTER TABLE `images_commands` DISABLE KEYS */;
INSERT INTO `images_commands` VALUES (1,1);
/*!40000 ALTER TABLE `images_commands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL,
  `blacklist` tinyint(4) NOT NULL,
  `role` varchar(255) NOT NULL,
  `createAt` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `updateAt` datetime(6) DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-18 14:03:01
