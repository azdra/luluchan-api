# LuluChan Api / Socket for Discord Bot

Steps to run this project:

### 1. Config .env
```bash
$ cp .env.dist .env
APP_DEV (true|false)
APP_PORT (Port of the api)

CLIENT_ID (Discord bot ID)
CLIENT_SECRETE (Discord bot Secret)

JWT_SECRET_KEY / JWT_PUBLIC_KEY (Dont touch)
JWT_PASSPHRASE (You custom secret code)

TYPEORM_TYPE (mariadb|mysql...)
TYPEORM_HOST (localhost)
TYPEORM_PORT (Database PORT)
TYPEORM_USERNAME (Database Username)
TYPEORM_PASSWORD (Database password)
TYPEORM_SYNCHRONIZE (true|false)
TYPEORM_LOGGING (true|false)
TYPEORM_ENTITIES [ "src/entity/**/*.ts" ]
```

### 2. Generate Public and Secret key
```bash
$ openssl genrsa -out src/Config/jwt/private.key -aes256 4096
[Enter pass phrase ...: It\'s your JWT_PASSPHRASE]

$ openssl rsa -pubout -in src/Config/jwt/private.key -out src/Config/jwt/public.key
[Enter pass phrase ...: It\'s your JWT_PASSPHRASE]
```
