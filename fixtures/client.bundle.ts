import Client from '../src/entity/Client';
import {fixture} from 'typeorm-fixture-builder';

export const client = fixture(Client, {
  id: '767824187779710986',
  activityName: 'It\'s time to become a Chinese farmer',
  activityType: 'PLAYING',
  status: 'idle',
});
