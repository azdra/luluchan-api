import {Entity, Column, PrimaryColumn} from 'typeorm';

export type ActivityTypes = 'PLAYING'|'STREAMING'|'LISTENING'|'WATCHING'|'CUSTOM'|'COMPETING'

export type ClientPresenceStatus = 'online' | 'idle' | 'dnd';
export type PresenceStatusData = ClientPresenceStatus | 'invisible';

/**
 * Class entity Client
 */
@Entity()
class Client {
  @PrimaryColumn()
  id: string;

  @Column({type: 'varchar', length: 255, nullable: false})
  activityName: string;

  @Column({type: 'varchar', length: 255, nullable: false})
  activityType: ActivityTypes;

  @Column({type: 'varchar', length: 255, nullable: false})
  status: ClientPresenceStatus;
}

export default Client;
