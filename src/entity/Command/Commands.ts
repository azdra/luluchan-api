import {
  Entity,
  Column,
  Unique,
  ManyToMany,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn, OneToMany,
} from 'typeorm';
import Images from './Images';
import CommandsType from './CommandsType';
import CommandsReaction from './CommandsReaction';

/**
 * Class entity Commands
 */
@Entity()
@Unique(['name'])
class Commands {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({
    default: 0,
  })
  uses: number;

  @Column({
    default: true,
    type: 'boolean',
  })
  visible: boolean;

  @ManyToOne(() => CommandsType, (commandsType) => commandsType.command, {
    nullable: false,
  })
  @JoinColumn({name: 'type'})
  commandType: CommandsType[];

  @ManyToMany(() => Images, (images) => images.commands)
  images: Images[]

  @OneToMany(() => CommandsReaction, (commandReactions) => commandReactions.command)
  commandReactions: CommandsReaction[]
}

export default Commands;
