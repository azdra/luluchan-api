import {
  Entity,
  Unique,
  PrimaryGeneratedColumn,
  OneToMany,
  Column,
} from 'typeorm';
import Commands from './Commands';

/**
 * Class entity CommandsType
 */
@Entity()
@Unique(['type'])
class CommandsType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: string

  @OneToMany(() => Commands, (commands) => commands.commandType)
  command: Commands
}

export default CommandsType;
