import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import Commands from './Commands';

/**
 * Entity CommandsImages
 */
@Entity()
class Images {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  path: string;

  @Column()
  size: number;

  @Column()
  format: string;

  @Column()
  name: string;

  @ManyToMany(() => Commands)
  @JoinTable({
    name: 'images_commands',
  })
  commands: Commands[];
}
export default Images;
