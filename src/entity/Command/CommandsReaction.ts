import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Commands from './Commands';

@Entity()
class CommandsReaction {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  author: string

  @Column()
  member: string

  @Column({default: 1})
  uses: number

  @ManyToOne(() => Commands, (command) => command.commandReactions)
  @JoinColumn()
  command: Commands
}

export default CommandsReaction;
