import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

/**
 * Class entity User
 */
@Entity()
class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  user: string;

  @Column('bool')
  blacklist: boolean;

  @Column('varchar')
  role: 'USER'|'ADMIN'|'SUPER_ADMIN'|'BOT';

  @CreateDateColumn()
  createAt: Date

  @UpdateDateColumn({nullable: true})
  updateAt?: Date
}

export default User;
