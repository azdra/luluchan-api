import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
} from 'typeorm';
import Guilds from './Guilds';

/**
 * Class entity Settings
 */
@Entity()
class GuildsSettings {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({default: process.env.DEFAULT_PREFIX})
  prefix: string;

  @Column({default: process.env.DEFAULT_COLOR})
  color: string;

  @Column({default: process.env.DEFAULT_LANG})
  lang: string;

  @Column({type: 'boolean', default: false})
  log: boolean;

  @OneToOne(() => Guilds, (guild) => guild.setting, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  guild: Guilds
}

export default GuildsSettings;
