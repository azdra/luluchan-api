import {Column, Entity, OneToOne, PrimaryGeneratedColumn} from 'typeorm';
import GuildsLog from './GuildsLog';

@Entity()
export class GuildsLogLeave {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'boolean', default: 0})
  enabled: boolean;

  @Column({nullable: true})
  message?: string;

  @Column({nullable: true})
  channel?: string;

  @OneToOne(() => GuildsLog, (GuildsLog) => GuildsLog.logLeave)
  log: GuildsLog;
}
