import {
  Column,
  Entity, JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Guilds from '../Guilds';
import {GuildsLogJoin} from './GuildsLogJoin';
import {GuildsLogLeave} from './GuildsLogLeave';

/**
 * Entity GuildLog
 */
@Entity()
class GuildsLog {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => Guilds, (guilds) => guilds.log, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  guild: Guilds

  @Column({nullable: true})
  other: string;

  @OneToOne(() => GuildsLogJoin, (GuildsLogJoin) => GuildsLogJoin.log)
  @JoinColumn()
  logJoin: GuildsLogJoin;

  @OneToOne(() => GuildsLogLeave, (GuildsLogLeave) => GuildsLogLeave.log)
  @JoinColumn()
  logLeave: GuildsLogLeave;
}

export default GuildsLog;
