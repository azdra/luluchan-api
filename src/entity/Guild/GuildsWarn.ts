import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import Guilds from './Guilds';

/**
 * Class entity GuildsWarn
 */
@Entity()
class GuildsWarn {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Guilds, (guild) => guild.audit, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({name: 'guild'})
  guild: Guilds;

  @Column()
  user: string;

  @Column()
  moderator: string;

  @Column({nullable: false})
  case: number;

  @Column('varchar', {
    length: 100,
  })
  reason: string;

  @CreateDateColumn()
  createAt: Date

  @UpdateDateColumn({nullable: true})
  updateAt?: Date

  @Column({type: 'boolean', default: false})
  delete: boolean
}

export default GuildsWarn;
