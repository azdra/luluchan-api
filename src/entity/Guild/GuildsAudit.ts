import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
} from 'typeorm';
import Guilds from './Guilds';

/**
 * Entity Log
 */
@Entity()
class GuildsAudit {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => Guilds, (guild) => guild.audit, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  guild: Guilds

  @Column('boolean', {default: false})
  guildUpdate: boolean;

  @Column('boolean', {default: false})
  channelCreate: boolean;

  @Column('boolean', {default: false})
  channelUpdate: boolean;

  @Column('boolean', {default: false})
  channelDelete: boolean;

  @Column('boolean', {default: false})
  channelOverwriteCreate: boolean;

  @Column('boolean', {default: false})
  channelOverwriteUpdate: boolean;

  @Column('boolean', {default: false})
  channelOverwriteDelete: boolean;

  @Column('boolean', {default: false})
  memberKick: boolean;

  @Column('boolean', {default: false})
  memberPrune: boolean;

  @Column('boolean', {default: false})
  memberBanAdd: boolean;

  @Column('boolean', {default: false})
  memberBanRemove: boolean;

  @Column('boolean', {default: false})
  memberUpdate: boolean;

  @Column('boolean', {default: false})
  memberRoleUpdate: boolean;

  @Column('boolean', {default: false})
  memberMove: boolean;

  @Column('boolean', {default: false})
  memberDisconnect: boolean;

  @Column('boolean', {default: false})
  botAdd: boolean;

  @Column('boolean', {default: false})
  roleCreate: boolean;

  @Column('boolean', {default: false})
  roleUpdate: boolean;

  @Column('boolean', {default: false})
  roleDelete: boolean;

  @Column('boolean', {default: false})
  inviteCreate: boolean;

  @Column('boolean', {default: false})
  inviteDelete: boolean;

  @Column('boolean', {default: false})
  webhookCreate: boolean;

  @Column('boolean', {default: false})
  webhookUpdate: boolean;

  @Column('boolean', {default: false})
  webhookCDelete: boolean;

  @Column('boolean', {default: false})
  emojiCreate: boolean;

  @Column('boolean', {default: false})
  emojiUpdate: boolean;

  @Column('boolean', {default: false})
  emojiDelete: boolean;

  @Column('boolean', {default: false})
  messageDelete: boolean;

  @Column('boolean', {default: false})
  messageBulkDelete: boolean;

  @Column('boolean', {default: false})
  messagePin: boolean;

  @Column('boolean', {default: false})
  messageUnPin: boolean;

  @Column('boolean', {default: false})
  integrationCreate: boolean;

  @Column('boolean', {default: false})
  integrationUpdate: boolean;

  @Column('boolean', {default: false})
  integrationDelete: boolean;
}

export default GuildsAudit;
