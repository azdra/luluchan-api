import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  JoinColumn,
  OneToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import GuildsSettings from './GuildsSettings';
import GuildsAudit from './GuildsAudit';
import GuildsLog from './Log/GuildsLog';
import GuildsWarn from './GuildsWarn';

/**
 * Entity of class Guild
 */
@Entity()
class Guilds {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  guild: string;

  @OneToOne(() => GuildsSettings, (guildsSettings) => guildsSettings.guild, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({name: 'setting'})
  setting: GuildsSettings;

  @OneToOne(() => GuildsAudit, (guildsAudit) => guildsAudit.guild, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({name: 'audit'})
  audit: GuildsAudit;

  @OneToOne(() => GuildsLog, (guildsLog) => guildsLog.guild, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({name: 'log'})
  log: GuildsLog;

  @OneToMany(() => GuildsWarn, (guildsWarn) => guildsWarn.guild, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  warn: GuildsWarn[];

  @CreateDateColumn()
  createAt: Date

  @UpdateDateColumn({nullable: true})
  updateAt?: Date
}

export default Guilds;
