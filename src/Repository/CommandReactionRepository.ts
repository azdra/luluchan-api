import {EntityRepository, Repository} from 'typeorm';
import CommandsReaction from '../entity/Command/CommandsReaction';

@EntityRepository(CommandsReaction)
export class CommandReactionRepository extends Repository<CommandsReaction> {
  /**
   * @param {string} author
   * @param {string} member
   * @param {Commands} command
   * @return {Promise<CommandsReaction>}
   */
  findReactionByUsersAndCommand(
      author: string,
      member: string,
      command: string,
  ): Promise<CommandsReaction> {
    return this.createQueryBuilder('reaction')
        .leftJoinAndSelect('reaction.command', 'command')
        .andWhere('reaction.author = :author AND reaction.member = :member OR reaction.author = :member AND reaction.member = :author')
        .andWhere('command.name = :commandName')
        .setParameters({
          author: author,
          member: member,
          commandName: command,
        })
        .getOne()
    ;
  }
}
