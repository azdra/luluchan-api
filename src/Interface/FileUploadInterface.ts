export interface FileUploadInterface {
  name: string,
  data: Buffer,
  size: number,
  encoding: string,
  temFilePath: string,
  truncated: boolean,
  mimetype: string,
  md5: string,
  mv: Function
}
