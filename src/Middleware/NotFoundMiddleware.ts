import {ExpressMiddlewareInterface, Middleware} from 'routing-controllers';
import {NextFunction, Request, Response} from 'express';
import * as debug from 'debug';

/**
 * Ckaass
 */
@Middleware({type: 'after'})
class NotFoundMiddleware implements ExpressMiddlewareInterface {
  private logger: debug.IDebugger = debug('project:middleware:FinalMiddleware');

  /**
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  public use(req: Request, res: Response, next?: NextFunction): void {
    if (!res.headersSent) {
      res.status(404).json({
        noobs: 'nnobs',
      });
    }
    res.end();
  }
}

export default NotFoundMiddleware;
