import * as jwt from 'jsonwebtoken';
import * as messageError from '../Config/messageError';
import {publicKey} from '../index';
import {UnauthorizedError} from 'routing-controllers';

/**
 * AuthMiddleware
 * @param {string} token
 * @param {string[]} roles
 * @constructor
 */
const AuthMiddleware = async (token: string, roles: string[]) => {
  if (process.env.APP_DEV.toString() === 'true') return true;
  if (!token) throw new UnauthorizedError(messageError['UnauthorizedError']);
  const bearerToken = token.split(' ')[1];
  let jwtValid: boolean = true;
  jwt.verify(bearerToken, publicKey, {
    algorithm: 'RS256',
  }, (err, decodeJwt: any) => {
    if (err) jwtValid = false;
  });
  return jwtValid;
};

export default AuthMiddleware;

