import * as dotenv from 'dotenv'; dotenv.config();
import 'reflect-metadata';
import * as fileUpload from 'express-fileupload';
import * as cors from 'cors';
import * as express from 'express';
import * as fs from 'fs';
import * as bodyParser from 'body-parser';
import ApiErrorHandlerMiddleware from './Middleware/ApiErrorHandlerMiddleware';
import NotFoundMiddleware from './Middleware/NotFoundMiddleware';
import * as messageError from './Config/messageError';
import AuthMiddleware from './Middleware/AuthMiddleware';
import {createServer} from 'http';
import {createConnection} from 'typeorm';
import {Action, HttpError, useExpressServer} from 'routing-controllers';
import * as Sentry from '@sentry/node';
import * as Tracing from '@sentry/tracing';

export const PORT = process.env.APP_PORT || 3000;
export const API_ROUTE = process.env.ROOT_PREFIX || '/api';
export const privateKey = fs.readFileSync(__dirname+process.env.JWT_SECRET_KEY);
export const publicKey = fs.readFileSync(__dirname+process.env.JWT_PUBLIC_KEY);
export const getProjectDir = () => __dirname;
export const getPublicDir = () => getProjectDir()+'/../public/';

const app = express();
const server = createServer(app);

app.use(fileUpload({
  createParentPath: true,
  limits: {
    fileSize: 10 * 1024 * 1024,
  },
}));

Sentry.init({
  dsn: 'https://3bc51a4532644ba8a27feee2a80021c7@o956116.ingest.sentry.io/5905478',
  integrations: [
    new Sentry.Integrations.Http({tracing: true}),
    new Tracing.Integrations.Express({app}),
  ],
  tracesSampleRate: 1.0,
});

app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

createConnection({
  type: 'mariadb',
  host: process.env.TYPEORM_HOST,
  port: Number(process.env.TYPEORM_PORT),
  database: process.env.TYPEORM_DATABASE,
  password: process.env.TYPEORM_PASSWORD,
  username: process.env.TYPEORM_USERNAME,
  synchronize: process.env.TYPEORM_SYNCHRONIZE.toString() === 'true',
  logging: process.env.TYPEORM_LOGGING.toString() === 'true',
  entities: ['src/entity/**/*.ts'],
}).then(() => {
  useExpressServer(app, {
    development: process.env.APP_DEV.toString() === 'true',
    defaultErrorHandler: false,
    middlewares: [NotFoundMiddleware, ApiErrorHandlerMiddleware],
    authorizationChecker: async (action: Action, roles: string[]) => {
      const authorizationToken = action.request.headers['authorization'];
      const middleware = await AuthMiddleware(authorizationToken, roles);
      if (!middleware) throw new HttpError(403, messageError['ForbiddenError']);
      return middleware;
    },
    routePrefix: process.env.ROOT_PREFIX,
    controllers: [__dirname + '/controllers/*'],
  });

  server.listen(PORT, () => {
    console.log('API started on localhost:%s', PORT+API_ROUTE);
  });
}).catch((e) => console.log(e));

app.use(Sentry.Handlers.errorHandler());
