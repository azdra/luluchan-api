const MessageError = {
  'ForbiddenError': 'Access Denied, You do not have access to this resource',
  'UnauthorizedError': 'Access Unauthorized',
};

export default MessageError;
