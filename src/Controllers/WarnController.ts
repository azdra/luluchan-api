import {
  Authorized,
  Body,
  Get,
  JsonController,
  Param,
  Post,
} from 'routing-controllers';
import {getRepository} from 'typeorm';
import GuildsWarn from '../entity/Guild/GuildsWarn';
import Guilds from '../entity/Guild/Guilds';

/**
 * Controller Warn
 */
@Authorized('SUPER_ADMIN')
@JsonController('/warns')
export class WarnController {
  private warnsRepository = getRepository<GuildsWarn>(GuildsWarn);
  private guildRepository = getRepository<Guilds>(Guilds);

  /**
   * salon
   * @param {string} id
   * @param {any} body
   */
  @Post('/:id')
  async add(@Param('id') id: string, @Body() body: any) {
    const guild = await this.guildRepository.createQueryBuilder('guild')
        .leftJoinAndSelect('guild.warn', 'warn')
        .andWhere('guild.guild = :guildId')
        .setParameters({
          guildId: id,
        })
        .getOneOrFail()
    ;

    const warn = new GuildsWarn();
    warn.guild = guild;
    warn.createAt = new Date;
    warn.user = body.user;
    warn.moderator = body.moderator;
    warn.reason = body.reason;
    warn.case = guild.warn.length+1;

    await this.warnsRepository.save(warn);

    delete warn.guild;

    return warn;
  }

  /**
   *
   * @param {string} guildId
   * @param {string} member
   * @param {number} page
   */
  @Get('/:guild/:member/:page')
  async getByGuildIdAndMemberId(@Param('guild') guildId: string, @Param('member') member: string, @Param('member') page: number = 1) {
    // const perPage: number = 5;
    // const offset: number = perPage * (page - 1);

    return this.warnsRepository.createQueryBuilder('warn')
        .leftJoin('warn.guild', 'guild')
        .where('guild.guild = :guildId')
        .andWhere('warn.user = :userId')
        .andWhere('warn.delete = false')
        .orderBy('warn.createAt', 'DESC')
        // .skip(offset)
        // .take(limitPerPage)
        .setParameters({
          guildId: guildId,
          userId: member,
        })
        .getMany();
  }
}
