import {
  Get,
  JsonController,
  Req,
  Res,
  Session,
} from 'routing-controllers';
import axios from 'axios';
import {URLSearchParams} from 'url';
import {Request, Response} from 'express';

@JsonController('/oauth')
export class AuthController {
  /**
   *
   * @param {any} session
   * @param {any} request
   * @param {any} response
   * @return {any}
   */
  @Get('/callback')
  async discordLoginCallback(@Session() session: any, @Req() request: Request, @Res() response: Response) {
    const callbackUrl = request.protocol + '://' + request.get('host') + request.baseUrl + request.path;

    if (request.query && !request.query.code) {
      return response.status(404).json({
        code: 404,
        message: 'Missing code params in request.',
      });
    }

    const code: string = request.query.code.toString();

    return axios({
      url: 'https://discord.com/api/oauth2/token',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: new URLSearchParams({
        'client_id': encodeURIComponent(process.env.CLIENT_ID),
        'client_secret': encodeURIComponent(process.env.CLIENT_SECRETE),
        'grant_type': encodeURIComponent('authorization_code'),
        'code': encodeURIComponent(code),
        'redirect_uri': callbackUrl,
        'scope': encodeURIComponent('identify guilds'),
      }),
    }).then((res) => {
      console.log('cookie set');
      response.cookie('_token', res.data.access_token, {
        expires: new Date(Date.now() + (604800*1000)),
      }).send(`<script> setTimeout(() =>  window.close(), 1000) </script>`);
    }).catch((err) => {
      return response.status(err.response.status).json({
        code: err.response.status,
        message: err.response.data.error_description,
      });
    });
  }
}
