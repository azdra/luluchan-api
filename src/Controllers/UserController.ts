import {
  JsonController,
  Param,
  Body,
  Get,
  Post,
  Put,
  Delete, Authorized,
} from 'routing-controllers';
import User from '../entity/User';
import {getRepository} from 'typeorm';

/**
 * Class UserController
 */
@Authorized('SUPER_ADMIN')
@JsonController('/users')
export class UserController {
  private userRepository = getRepository(User);
  /**
   * Get all users
   * @return {string}
   */
  @Get('/')
  getAll() {
    return this.userRepository.find();
  }

  /**
   * Get one specific user
   * @param {number} id
   * @return {string|User}
   */
  @Get('/:id')
  async getOneById(@Param('id') id: number) {
    const findUser = await this.userRepository.findOne(id);
    return findUser || {error: `no user found for id ${id}`};
  }

  /**
   * Create new user
   * @param {User} user
   * @return {string}
   */
  @Post('/')
  add(@Body() user: User) {
    return this.userRepository.insert(user);
  }

  /**
   * Updating one user
   * @param {number} id
   * @param {User} user
   * @return {string}
   */
  @Put('/:id')
  updateById(@Param('id') id: number, @Body() user: User) {
    return this.userRepository.update(id, user);
  }

  /**
   * Delete one user
   * @param {number} id
   * @return {string}
   */
  @Delete('/:id')
  removeByID(@Param('id') id: number) {
    return this.userRepository.delete(id);
  }
}
