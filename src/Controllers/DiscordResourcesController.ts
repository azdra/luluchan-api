import {
  Body,
  JsonController,
  Post,
  Req,
  Res,
  Session,
} from 'routing-controllers';
import {Request, Response} from 'express';
import axios from 'axios';

@JsonController('/discord')
export class DiscordResourcesController {
  /**
   * @param {Body} body
   * @param {Session} session
   * @param {Req} req
   * @param {Res} res
   * @return {void}
   */
  @Post('/users/@me')
  async discordLogin(@Body() body: any, @Session() session: any, @Req() req: Request, @Res() res: Response) {
    if (req.body) {
      return axios({
        method: 'GET',
        url: 'https://discord.com/api/v8/users/@me',
        headers: {
          'authorization': `Bearer ${req.body.token}`,
          'Content-type': 'application/json',
        },
      }).then((result) => {
        const user = Object.assign({}, result.data);
        res.json(user);
      }).catch((err) => {
        res.json({
          message: err.response.data.message,
          code: err.response.data.code,
          status: err.response.status,
        });
      });
    }
  }
}
