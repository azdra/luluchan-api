import {
  Authorized,
  Get,
  JsonController,
  Param,
  Post,
  Req,
  Res,
} from 'routing-controllers';
import {Request, Response} from 'express';
import {FileUploadInterface} from '../Interface/FileUploadInterface';
import * as path from 'path';
import {getPublicDir} from '../index';
import {nanoid} from 'nanoid';
import {getRepository} from 'typeorm';
import Commands from '../entity/Command/Commands';
import CommandsType from '../entity/Command/CommandsType';
import Images from '../entity/Command/Images';

@Authorized('SUPER_ADMIN')
@JsonController('/img')
export class ImageController {
  private commandRepository = getRepository(Commands);
  private commandTypeRepository = getRepository(CommandsType);
  private imagesRepository = getRepository(Images);

  /**
   * Get one command name
   * @param {Request} request
   * @param {Response} response
   * @return {Promise<Response>}
   */
  @Post('/uploads')
  async upload(@Req() request: Request, @Res() response: Response) {
    try {
      const typeId: number = request.body.type;
      const commandId: number = request.body.command;
      const _files: any = request.files;

      try {
        for (const filesKey in _files) {
          if (_files.hasOwnProperty(filesKey)) {
            const image: FileUploadInterface = _files[filesKey];

            const imageSize: number = image.size;

            if (imageSize / (1024*1024) > 10) {
              return response.json({
                message: 'The size of file is greater than 10 MB/MO',
              }).status(413);
            }

            if (image) {
              const extname = path.extname(image.name);

              if (!extname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
                return response.json({
                  message: 'Unsupported Media Type',
                }).status(415);
              }

              const commandType: CommandsType = await this.commandTypeRepository.findOneOrFail(typeId);
              const command: Commands = await this.commandRepository.findOneOrFail(commandId);

              if (!commandType || !command) {
                return response.json({
                  message: 'Command or CommandType is null',
                }).status(400);
              }

              const name = nanoid();

              const imagePath = `${commandType.type}/${command.name}/${name+extname}`;

              image.mv(`${getPublicDir()}/${imagePath}`);

              const newImage = new Images();
              newImage.format = image.mimetype;
              newImage.size = image.size;
              newImage.path = imagePath;
              newImage.name = name;
              newImage.commands = [];
              newImage.commands.push(command);

              await this.imagesRepository.save(newImage);
              console.log(`${imagePath} SAVED`);
            }
          }
        }
        console.log('END');
        return response.json({
          message: 'The file has been successfully uploaded!',
        });
      } catch (e) {
        return response.json({
          message: 'The file is missing',
        }).status(400);
      }
    } catch (e) {
      console.log('ERROR');
    }
  }

  /**
   * @param {string} name
   * @param {Request} request
   * @param {Response} response
   */
  @Get('/:name')
  async get(@Param('name') name: string, @Req() request: Request, @Res() response: Response) {
    const command = await this.commandRepository.findOne({
      name: name,
    });

    if (command) {
      const images = await this.imagesRepository.createQueryBuilder('image')
          .leftJoinAndSelect('image.commands', 'command')
          .andWhere('command.id = :id')
          .setParameters({
            id: command.id,
          })
          .getMany();

      if (Array.isArray(images) && images.length >= 1) {
        const rd = images[Math.floor(Math.random() * images.length)];
        return response.json({
          name: rd.name,
          url: 'https://cdn.lulu-chan.fun/'+rd.path,
        });
      } else {
        return response.status(404);
      }
    } else {
      return response.status(404);
    }
  }
}
