import {
  JsonController,
  Get,
  Authorized,
  Post,
  Body,
  Param,
} from 'routing-controllers';
import CommandsType from '../entity/Command/CommandsType';
import {getRepository} from 'typeorm';

@Authorized('SUPER_ADMIN')
@JsonController('/command/types')
export class CommandsTypeController {
  private commandsTypeRepository = getRepository(CommandsType);

  /**
   * @return {commandsTypeRepository}
   */
  @Get('/')
  getAll() {
    return this.commandsTypeRepository.createQueryBuilder('type')
        .leftJoinAndSelect('type.command', 'command')
        .getMany();
  }

  /**
   * @param {string} type
   * @return {commandsTypeRepository}
   */
  @Get('/:type')
  getByType(@Param('type') type: string) {
    return this.commandsTypeRepository.findOneOrFail({
      type: type,
    });
  }

  /**
   * @param {CommandsType} commandsType
   * @return {commandsTypeRepository}
   */
  @Post('/')
  add(@Body() commandsType: CommandsType) {
    return this.commandsTypeRepository.insert(commandsType);
  }
}
