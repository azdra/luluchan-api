import {
  JsonController,
  Authorized,
  Post,
} from 'routing-controllers';
import Client from '../entity/Client';
import {getRepository} from 'typeorm';

@Authorized('SUPER_ADMIN')
@JsonController('/client')
export class ClientController {
  private clientRepository = getRepository(Client);

  /**
   * Insert client data
   * @return {clientRepository}
   */
  @Post('/')
  async add() {
    // Check if client already created
    const discordClient = await this.clientRepository.findOne({id: process.env.CLIENT_ID});
    if (discordClient) return discordClient;

    // Create the client if isn't created
    const client = new Client();
    client.activityName = 'It\'s time to become a Chinese farmer';
    client.status = 'idle';
    client.activityType = 'PLAYING';
    client.id = process.env.CLIENT_ID;
    await this.clientRepository.save(client);

    return client;
  }
}
