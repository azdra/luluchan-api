import {
  JsonController,
  Get,
  Post,
  Body,
  Param,
  Authorized,
} from 'routing-controllers';
import Commands from '../entity/Command/Commands';
import {getRepository} from 'typeorm';
import CommandsType from '../entity/Command/CommandsType';

@Authorized('SUPER_ADMIN')
@JsonController('/commands')
export class CommandsController {
  private commandRepository = getRepository(Commands);
  private commandTypeRepository = getRepository(CommandsType);

  /**
   * getAllCommand
   * @return {Promise<CommandsType[]>}
   */
  @Get('/')
  getAllCommand(): Promise<CommandsType[]> {
    return this.commandTypeRepository.createQueryBuilder('commandType')
        .leftJoinAndSelect('commandType.command', 'command')
        .orderBy('command.type', 'ASC')
        .getMany();
  }

  /**
   * getAllCommandType
   * @return {Promise<CommandsType[]>}
   */
  @Get('/types')
  getAllType(): Promise<CommandsType[]> {
    return this.commandTypeRepository.find();
  }

  /**
   * getCommandByType
   * @param {number} type
   * @return {Promise<Commands[]>}
   */
  @Get('/types/:type')
  getCommandByType(@Param('type') type): Promise<Commands[]> {
    return this.commandRepository.createQueryBuilder('command')
        .leftJoin('command.commandType', 'commandType')
        .andWhere('commandType.id = :type', {type})
        .orderBy('command.name', 'ASC')
        .getMany();
  }

  /**
   * Get one command name
   * @param {string} name
   * @return {Promise<Commands[]>}
   */
  @Get('/:name')
  getOneCommandByName(@Param('name') name: string): Promise<Commands> {
    return this.commandRepository.findOne({
      name: name,
    });
  }

  /**
   * Insert new command
   * @param {Commands} commands
   * @return {commandRepository}
   */
  @Post('/')
  addCommand(@Body() commands: Commands) {
    return this.commandRepository.insert(commands);
  }
}
