import {Authorized, Body, JsonController, Post, Req, Res} from 'routing-controllers';
import {getCustomRepository, getRepository} from 'typeorm';
import {CommandReactionRepository} from '../Repository/CommandReactionRepository';
import CommandsReaction from '../entity/Command/CommandsReaction';
import {Request, Response} from 'express';
import Commands from '../entity/Command/Commands';

@Authorized('SUPER_ADMIN')
@JsonController('/command-reaction')
export class CommandReactionController {
  private commandReactionRepository = getCustomRepository<CommandReactionRepository>(CommandReactionRepository);
  private commandRepository = getRepository<Commands>(Commands);

  @Post('/')
  async createOrUpdateReaction(
    @Body() commandReaction: CommandsReaction,
    @Req() request: Request,
    @Res() response: Response,
  ) {
    const command = await this.commandRepository.findOne({
      name: String(commandReaction.command),
    });

    if (!command) {
      return response.status(404).json({
        code: 'commandNotFound',
        message: 'Command Not Found',
      });
    }

    let reaction = await this.commandReactionRepository.findReactionByUsersAndCommand(
        commandReaction.author,
        commandReaction.member,
        command.name,
    );

    if (!reaction) {
      reaction = new CommandsReaction();
      reaction.author = commandReaction.author;
      reaction.member = commandReaction.member;
      reaction.command = command;
      await this.commandReactionRepository.insert(reaction);
    } else {
      reaction.uses += 1;
      await this.commandReactionRepository.save(reaction);
    }


    return response.json(reaction);
  }
}
