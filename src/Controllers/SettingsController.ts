import {
  Authorized,
  Body,
  Get,
  JsonController,
  Param,
  Post,
  Put, Res,
} from 'routing-controllers';
import {getRepository} from 'typeorm';
import GuildsSettings from '../entity/Guild/GuildsSettings';
import Guilds from '../entity/Guild/Guilds';
import GuildsAudit from '../entity/Guild/GuildsAudit';
import GuildsLog from '../entity/Guild/Log/GuildsLog';
import {Response} from 'express';
import {GuildsLogJoin} from '../entity/Guild/Log/GuildsLogJoin';
import {GuildsLogLeave} from '../entity/Guild/Log/GuildsLogLeave';

/**
 * CLass SettingsController
 */
@Authorized('SUPER_ADMIN')
@JsonController('/settings')
export class SettingsController {
  private settingRepository = getRepository<GuildsSettings>(GuildsSettings)
  private guildRepository = getRepository<Guilds>(Guilds)
  private guildAuditRepository = getRepository<GuildsAudit>(GuildsAudit)
  private guildLogRepository = getRepository<GuildsLog>(GuildsLog)
  private guildLogJoinRepository = getRepository<GuildsLogJoin>(GuildsLogJoin)
  private guildLogLeftRepository = getRepository<GuildsLogLeave>(GuildsLogLeave)
  private guildSettingsRepository = getRepository<GuildsSettings>(GuildsSettings)

  /**
   * Get the settings
   * @param {number} id
   * @param {Post} post
   * @return {settingRepository}
   */
  @Post('/:id')
  async getOneByIdOrCreate(@Param('id') id: string, @Body() post) {
    // CHECK IF DATA WHIT SPECIFIC ID ALREADY EXIST
    const guildSettings = await this.guildRepository.findOne({
      relations: ['setting', 'log'],
      where: [
        {
          guild: id,
        },
      ],
    });

    // RETURN DATA IF DATA EXIST
    if (guildSettings) {
      return guildSettings;
    }

    // CREATE NEW ENTRY GUILD AUDIT
    const guildAudit = new GuildsAudit();

    const guildLogJoin = new GuildsLogJoin();
    const guildLogLeave = new GuildsLogLeave();

    // CREATE NEW ENTRY GUILD LOG
    const guildLog = new GuildsLog();
    guildLog.logJoin = guildLogJoin;
    guildLog.logLeave = guildLogLeave;

    // CREATE NEW ENTRY GUILD SETTINGS
    const guildsSettings = new GuildsSettings();

    // CREATE NEW ENTRY GUILD WHIT RELATIONSHIP
    const dataGuild = new Guilds();
    dataGuild.guild = id;
    dataGuild.setting = guildsSettings;
    dataGuild.audit = guildAudit;
    dataGuild.log = guildLog;

    // SAVE THE ENTRIES
    await this.guildAuditRepository.save(guildAudit);
    await this.guildLogJoinRepository.save(guildLogJoin);
    await this.guildLogLeftRepository.save(guildLogLeave);
    await this.guildLogRepository.save(guildLog);
    await this.guildSettingsRepository.save(guildsSettings);
    await this.guildRepository.save(dataGuild);

    // RETURN DATA CREATE
    return dataGuild;
  }

  /**
   * update settings
   * @param {number} id
   * @param {GuildsSettings} settings
   * @param {Response} response
   * @return {settingRepository}
   */
  @Put('/:id')
  async updateById(@Param('id') id: string, @Body() settings: GuildsSettings, @Res() response: Response) {
    const repoSettings = await this.guildRepository.findOne({
      relations: ['setting'],
      where: [{guild: id}],
    });
    await this.settingRepository.update(repoSettings.setting.id, settings);
    return response.json({
      'settingUpdateSuccess': 'T',
    });
  }

  @Get('/:id/log')
  async getLogByGuild(@Param('id') id: string, @Res() response: Response) {
    const log = await this.guildLogRepository.createQueryBuilder('log')
        .leftJoinAndSelect('log.logJoin', 'logJoin')
        .leftJoinAndSelect('log.logLeave', 'logLeave')
        .leftJoin('log.guild', 'guild')
        .where('guild.guild = :guildId')
        .setParameter('guildId', id)
        .getOne();
    return response.json(log);
  }

  @Put('/:id/log')
  async putLogByGuild(@Param('id') id: string, @Body() settings: GuildsLog, @Res() response: Response) {
    const log = await this.guildLogRepository.createQueryBuilder('log')
        .leftJoin('log.guild', 'guild')
        .where('guild.guild = :guildId')
        .setParameter('guildId', id)
        .getOne();
    await this.guildLogRepository.update(log, settings);
  }

  @Put('/:id/log/join')
  async putLogJoinByGuild(@Param('id') id: string, @Body() settings: GuildsLogJoin, @Res() response: Response) {
    const log = await this.guildLogRepository.createQueryBuilder('log')
        .leftJoin('log.guild', 'guild')
        .leftJoinAndSelect('log.logJoin', 'logJoin')
        .where('guild.guild = :guildId')
        .setParameter('guildId', id)
        .getOne();
    await this.guildLogJoinRepository.update(log.logJoin, settings);
  }
}
